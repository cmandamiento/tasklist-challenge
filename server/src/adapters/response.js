const randomUuid = (id) => parseInt(Math.random() * (1000000 + id));

const ServiceResponse = ({ success, data, error }) => ({
  success: success === undefined ? !error : !!success,
  data,
  error,
});

const ImplementsTasksUuid = (tasks) =>
  tasks.map((task, id) => ({ uuid: randomUuid(id), title: task }));

module.exports = {
  ServiceResponse,
  ImplementsTasksUuid,
};
