const fetch = require("node-fetch");
const {
  ServiceResponse,
  ImplementsTasksUuid,
} = require("../adapters/response");
const DEFAULT_TASKS_LIMIT = 3;
const REMOTE_API = "https://lorem-faker.vercel.app/api";

const fetchTasks = async (req, res) => {
  const limit = req.params?.limit || DEFAULT_TASKS_LIMIT;
  const url = `${REMOTE_API}?quantity=${limit}`;

  try {
    const data = await fetch(url);
    const tasks = await data.json();

    res.send(
      ServiceResponse({
        success: true,
        data: ImplementsTasksUuid(tasks),
      })
    );
  } catch (error) {
    res.status(400).send(ServiceResponse({ error }));
  }
};

const updateTask = (req, res) => {
  const uuid = req.params?.uuid;

  if (!uuid) {
    res.status(400).send(ServiceResponse({ error: "uuid is required" }));
  }

  const message = `${uuid} was completed`;

  console.log(message);
  res.send(ServiceResponse({ data: message }));
};

module.exports = {
  fetchTasks,
  updateTask,
};
