const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
const { tasksController } = require("./controllers");

const app = express();
const port = 9000;

/* App setup */
app.use(helmet());
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

/* Routes */
app.get("/tasks/:limit?", tasksController.fetchTasks);
app.put("/tasks/:uuid?", tasksController.updateTask);

/* Log */
app.listen(port, () => console.log(`Running at http://localhost:${port}`));
