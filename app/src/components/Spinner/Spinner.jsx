import { useAppContext } from "../../context/AppContext";
import "./Spinner.css";

const Spinner = () => {
  const {
    state: { isLoading },
  } = useAppContext();

  return isLoading ? (
    <div className="spinner">
      <div className="spinner-border text-info" role="status">
        <span className="visually-hidden">Loading...</span>
      </div>
    </div>
  ) : (
    <></>
  );
};

export default Spinner;
