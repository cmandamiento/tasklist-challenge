import "./Task.css";

const Task = ({ uuid, title, completed, onClick }) => (
  <div className={`card task ${completed && "is-completed"}`} onClick={onClick}>
    <div className="card-body">
      <div className="card-title mb-2 text-muted">
        <code>{uuid}</code>
      </div>
      <div className="card-subtitle">{title}</div>
    </div>
  </div>
);

export default Task;
