import "./Modal.css";

const ModalConfirm = ({
  children,
  confirmText,
  cancelText,
  onConfirm,
  onCancel,
}) => (
  <div className="app-modal" data-theme="light">
    <div className="app-modalContent">
      <div>{children}</div>
      <div className="d-grid gap-2">
        {!!onConfirm && (
          <button
            className="btn btn-primary mr-1"
            onClick={onConfirm}
            type="button"
          >
            {confirmText || "Confirm"}
          </button>
        )}
        {!!onCancel && (
          <button
            className="btn btn-secondary"
            onClick={onCancel}
            type="button"
          >
            {cancelText || "Cancel"}
          </button>
        )}
      </div>
    </div>
  </div>
);

export default ModalConfirm;
