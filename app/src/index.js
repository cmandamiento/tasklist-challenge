import ReactDOM from "react-dom";
import { TaskListApp } from "./App";

const app = document.getElementById("app");
ReactDOM.render(<TaskListApp />, app);
