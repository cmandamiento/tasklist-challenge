import React, { useEffect } from "react";
import { render } from "react-dom";
import Spinner from "./components/Spinner/Spinner";
import { AppProvider, useAppContext } from "./context/AppContext";
import Home from "./pages/Home";
import axios from "axios";
import { setLoading } from "./context/actions";

function App() {
  const { dispatch } = useAppContext();

  useEffect(() => {
    axios.interceptors.request.use(
      function (config) {
        dispatch(setLoading(true));
        return config;
      },
      function (error) {
        return Promise.reject(error);
      }
    );

    axios.interceptors.response.use(
      function (response) {
        dispatch(setLoading(false));
        return response;
      },
      function (error) {
        return Promise.reject(error);
      }
    );
  }, []);

  return (
    <>
      <Spinner />
      <Home />
    </>
  );
}

export function TaskListApp() {
  return (
    <AppProvider>
      <App />
    </AppProvider>
  );
}
