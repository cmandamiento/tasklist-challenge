import axios from "axios";
import { API_URL } from "./constants";

export const fetchTasks = async (limit = "") => {
  const { data } = await axios.get(`${API_URL}/tasks/${limit}`);
  return data.data;
};

export const completeTask = async (uuid) => {
  const { data } = await axios.put(`${API_URL}/tasks/${uuid || ""}`, {});
  return data;
};
