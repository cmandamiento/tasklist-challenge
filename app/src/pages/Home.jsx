import React, { useState } from "react";
import { completeTask as serverCompleteTask, fetchTasks } from "../api/Tasks";
import ModalConfirm from "../components/Modal/Modal";
import Task from "../components/Task/Task";
import { completeTask, setTasks } from "../context/actions";
import { useAppContext } from "../context/AppContext";

const Home = () => {
  const [limit, setLimit] = useState("");
  const {
    state: { tasks },
    dispatch,
  } = useAppContext();
  const [showModal, setShowModal] = useState(false);
  const [selectedTask, setSelectedTask] = useState(null);

  const handleSubmit = async (ev) => {
    ev.preventDefault();
    const tasks = await fetchTasks(limit);
    dispatch(setTasks(tasks));
  };

  const confirmTask = async () => {
    await serverCompleteTask(selectedTask.uuid);
    dispatch(completeTask(selectedTask.uuid));
    setShowModal(false);
  };

  const handleTask = (task) => {
    setShowModal(true);
    setSelectedTask(task);
  };

  return (
    <div>
      <div className="row py-3">
        <form onSubmit={handleSubmit} className="mb-3">
          <div className="mb-3">
            <label className="form-label" htmlFor="limit">
              Number of tasks
            </label>
            <input
              className="form-control"
              type="text"
              id="limit"
              name="limit"
              placeholder="Set a limit. By default: 3"
              onChange={(ev) => setLimit(ev?.target?.value)}
              value={limit}
            />
          </div>
          <div className="d-grid">
            <button className="btn btn-primary" type="submit">
              Submit
            </button>
          </div>
        </form>
      </div>
      {tasks.length > 0 && (
        <div className="row">
          <h2>Tasks ({tasks.length}):</h2>
          {tasks.map(({ uuid, title, completed }) => (
            <div key={uuid} className="col-12 col-md-4 mb-4">
              <Task
                completed={completed}
                uuid={uuid}
                title={title}
                onClick={() => !completed && handleTask({ uuid, title })}
              />
            </div>
          ))}
        </div>
      )}

      {showModal && (
        <ModalConfirm
          confirmText="Mark as done"
          onConfirm={confirmTask}
          onCancel={() => setShowModal(false)}
        >
          <h4>{`${selectedTask.uuid} - ${selectedTask.title}`}</h4>
        </ModalConfirm>
      )}
    </div>
  );
};

export default Home;
