export const actions = {
  SET_LOADING: "SET_LOADING",
  SET_TASKS: "SET_TASKS",
  COMPLETE_TASK: "COMPLETE_TASK",
};

export const setLoading = (payload) => ({ type: actions.SET_LOADING, payload });
export const setTasks = (payload) => ({ type: actions.SET_TASKS, payload });
export const completeTask = (payload) => ({
  type: actions.COMPLETE_TASK,
  payload,
});
