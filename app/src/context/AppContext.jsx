import React from "react";
import { actions } from "./actions";

const AppContext = React.createContext();
const initialState = {
  isLoading: false,
  tasks: [],
};

function appReducer(state, action) {
  switch (action.type) {
    case actions.SET_LOADING: {
      return { ...state, isLoading: action.payload };
    }

    case actions.SET_TASKS: {
      return { ...state, tasks: action.payload };
    }

    case actions.COMPLETE_TASK: {
      return {
        ...state,
        tasks: state.tasks.map((task) =>
          task.uuid === action.payload ? { ...task, completed: true } : task
        ),
      };
    }

    default: {
      return state;
    }
  }
}

function AppProvider({ children }) {
  const [state, dispatch] = React.useReducer(appReducer, initialState);
  const value = { state, dispatch };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
}

function useAppContext() {
  const context = React.useContext(AppContext);

  if (context === undefined) {
    throw new Error(`useAppContext must be inside AppContext.Provider`);
  }

  return context;
}

export { AppProvider, useAppContext };
