## TASKLIST App
### Steps
#### 1) Use nvm to set node version (v.14.17.4)
```console
foo@bar:~$nvm use
````

#### 2) Install server dependencies and run it
```console
foo@bar:~$cd server
foo@bar:~$npm install
foo@bar:~$npm start
````

#### 3) Install client dependencies and run it
```console
foo@bar:~$cd app
foo@bar:~$npm install
foo@bar:~$npm start
````

#### 4) Go to http://localhost:1234

